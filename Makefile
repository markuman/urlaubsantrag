dpl ?= deploy.env
include $(dpl)
export $(shell sed 's/=.*//' $(dpl))

VERSION=10


help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

# DOCKER TASKS

build: ## Build the container
	docker build -t $(CONTAINER_NAME):latest .

push: ## Push image to gitlab registry
	docker tag $(CONTAINER_NAME):latest  $(DOCKER_REPO)/$(CONTAINER_NAME):latest
	docker push $(DOCKER_REPO)/$(CONTAINER_NAME):latest

release: ## Make a relase (build and push version of package.json)
	make build
	docker tag $(CONTAINER_NAME):latest $(DOCKER_REPO)/$(CONTAINER_NAME):$(VERSION) 
	docker push $(DOCKER_REPO)/$(CONTAINER_NAME):$(VERSION) 

deploy: ## Run the image
	docker service create -p $(HOST_PORT):$(CONTAINER_PORT) --network=$(NETWORK_NAME) --name $(CONTAINER_NAME) --with-registry-auth $(DOCKER_REPO)/$(CONTAINER_NAME):$(VERSION)

update: ## Update service
	docker service update --name $(CONTAINER_NAME) --with-registry-auth $(CONTAINER_NAME):$(VERSION)