FROM alpine:3.6

COPY index.html /app/index.html
COPY enc.png /app/enc.png

EXPOSE 80
ENTRYPOINT busybox httpd -p 80 -h /app && tail -f /dev/null
